package utilities;

import java.io.IOException;

import org.json.JSONException;

public class MainLoadScript {

	public MainLoadScript() {

	}

	public static void main(String args[]) {
		try {

			/*
			System.out.println("addUser:" + UserFunctions.addUser());
			System.out.println("updateAge:" + UserFunctions.updateAge());
			System.out.println("updateSpendingLimit:" + UserFunctions.updateSpendingLimit());
			System.out.println("updateMusic:" + UserFunctions.updateMusic());
			System.out.println("updateAtmospheres:" + UserFunctions.updateAtmospheres());
			System.out.println("getUserGroups:" + UserFunctions.getUserGroups());
			System.out.println("searchForUsers:" + UserFunctions.searchForUsers());
			System.out.println("trendingList:" + UserFunctions.trendingList());

			System.out.println("***************************");

			System.out.println("setUserActivity:" + VenueFunctions.setUserActivity());
			System.out.println("reportVenueDetailsError:" + VenueFunctions.reportVenueDetailsError());
			System.out.println("addBuzz:" + VenueFunctions.addBuzz());
			System.out.println("getVenueDataForUser:" + VenueFunctions.getVenueDataForUser());
			System.out.println("getLatestBuzz:" + VenueFunctions.getLatestBuzz());
			System.out.println("getBuzz:" + VenueFunctions.getBuzz());
			System.out.println("getVenueBuzzImages:" + VenueFunctions.getVenueBuzzImages());

			System.out.println("***************************");

			System.out.println("addGroup:" + GroupFunctions.addGroup());
			System.out.println("joinAndLeaveGroup:" + GroupFunctions.joinAndLeaveGroup());
			System.out.println("inviteUserAndRespondToInvite:" + GroupFunctions.inviteUserAndRespondToInvite());
			System.out.println("addGroupBuzz:" + GroupFunctions.addGroupBuzz());
			System.out.println("getListOfUsersInGroup:" + GroupFunctions.getListOfUsersInGroup());
			System.out.println("searchForGroups:" + GroupFunctions.searchForGroups());
			System.out.println("popularGroups:" + GroupFunctions.popularGroups());
			System.out.println("getGroupInfo:" + GroupFunctions.getGroupInfo());
			System.out.println("trendingList:" + GroupFunctions.trendingList());
			System.out.println("getIncomingGroupRequests:" + GroupFunctions.getIncomingGroupRequests());
			System.out.println("getLatestBuzz:" + GroupFunctions.getLatestBuzz());
			System.out.println("getGroupBuzz:" + GroupFunctions.getGroupBuzz());
			System.out.println("getGroupBuzzImages:" + GroupFunctions.getGroupBuzzImages());
			 */
			LoadScriptHelper helper1 = new LoadScriptHelper();
			Thread t1 = new Thread(helper1);
			t1.start();

			LoadScriptHelper helper2 = new LoadScriptHelper();
			Thread t2 = new Thread(helper2);
			t2.start();

			LoadScriptHelper helper3 = new LoadScriptHelper();
			Thread t3 = new Thread(helper3);
			t3.start();



		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
